// tests/api/lib/types.ts

export interface UserDTO {
    id: number;
    email: string;
    userName: string; // Додали властивість userName
    // Додайте інші поля, які очікуєте отримувати з API
}
