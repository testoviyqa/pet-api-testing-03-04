import { expect } from "chai";
import { UsersController } from '../lib/controllers/users.controller';
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const usersController = new UsersController();

describe("Get All Users", () => {
    it("should return all users successfully", async () => {
        const response = await usersController.getAllUsers();

        // Checking the status code
        checkStatusCode(response, 200);

        // Checking that the response is an array
        expect(response.body).to.be.an('array');

        // Checking the first element of the array, if it exists
        if (response.body.length > 0) {
            expect(response.body[0]).to.include.keys('id', 'avatar', 'email', 'userName');
            // Additional checks for each key can be added here
        }
    });
});
