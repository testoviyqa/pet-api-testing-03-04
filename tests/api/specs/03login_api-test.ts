import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';

describe('User Login', function() {
    const authController = new AuthController();

    it('should login successfully with valid credentials', async function() {
        const email = 'gaHko@example.com'; // Replace with a valid email
        const password = 'Password123'; // Replace with a valid password

        const response = await authController.login(email, password);

        expect(response.statusCode).to.equal(200); // Changed from 'status' to 'statusCode'
        expect(response.body).to.have.property('token');
    });
});
