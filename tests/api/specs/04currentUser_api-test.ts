import { checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { expect } from 'chai';
import { v4 as uuidv4 } from 'uuid';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterController } from '../lib/controllers/register.controller';
import { UsersController } from '../lib/controllers/users.controller';

// Creating test data
const testUserData = {
  email: `test-${uuidv4()}@example.com`,
  password: 'TestPassword123',
  userName: `testUser-${uuidv4()}`,
  avatar: 'http://example.com/avatar.jpg'
};

describe('Current User API Test', () => {
  let token: string;
  const authController = new AuthController();
  const registerController = new RegisterController();
  const usersController = new UsersController();

  before(async () => {
    // User registration
    let response = await registerController.registerUser(testUserData);
    checkStatusCode(response, 201);

    // Authorization to obtain token
    const loginResponse = await authController.login(testUserData.email, testUserData.password);
    token = loginResponse.body.token.accessToken.token;
  });

  it('should get current user data', async () => {
    // Getting current user data
    const currentUserResponse = await usersController.getCurrentUser(token);

    // Assertions
    expect(currentUserResponse.statusCode).to.equal(200);
    expect(currentUserResponse.body.email).to.equal(testUserData.email);
    expect(currentUserResponse.body.userName).to.equal(testUserData.userName);
    // Additional assertions if needed
  });

  // After running the tests, you can delete the user if necessary
  after(async () => {
    // User deletion
    // Use the deleteUser method with the appropriate controller
  });
});
