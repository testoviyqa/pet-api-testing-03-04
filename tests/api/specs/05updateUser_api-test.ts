import { AuthController } from '../lib/controllers/auth.controller';
import { v4 as uuidv4 } from 'uuid';
import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from '../lib/controllers/register.controller';

// Creating test data
const testUserData = {
    email: `test-${uuidv4()}@example.com`,
    password: 'TestPassword123',
    userName: `testUser-${uuidv4()}`,
    avatar: 'http://example.com/avatar.jpg'
  };

describe('Update Current User', function() {
    const authController = new AuthController();
    const registerController = new RegisterController();
    let usersController: UsersController;
    let accessToken: string;
    let userDataBeforeUpdate, userDataToUpdate;

    before(async function() {

        // Assuming you have a way to obtain accessToken
        // accessToken = ...

        usersController = new UsersController();
        let response = await registerController.registerUser(testUserData);
        checkStatusCode(response, 201);

        // Authorization to obtain token
        const loginResponse = await authController.login(testUserData.email, testUserData.password);
        console.log(loginResponse.body.token);
        accessToken = loginResponse.body.token.accessToken.token;
    });

    it('should return correct details of the current user', async function() {
        const response = await usersController.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        userDataBeforeUpdate = response.body;
    });

    it('should update the current user successfully', async function() {
        // Update userDataToUpdate with appropriate data
        // ...

        const updateResponse = await usersController.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(updateResponse, 204);
    });

    it('should return correct user details by id after updating', async function() {
        const response = await usersController.getUserById(userDataBeforeUpdate.id);
        checkStatusCode(response, 200);
        expect(response.body).to.deep.equal(userDataToUpdate, "User details aren't correct");
    });

    // You can add afterEach or other hooks as needed
});
