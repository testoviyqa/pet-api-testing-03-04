import { expect } from "chai";
import { RegisterController } from '../lib/controllers/register.controller';
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const register = new RegisterController();

// Function to generate a random string
function generateRandomString(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

describe("User Registration", () => {
    const randomString = generateRandomString(5);
    const newUser = {
        avatar: "http://example.com/avatar.jpg",
        email: `${randomString}@example.com`,
        userName: `user${randomString}`,
        password: "Password123"
    };

    let userId; // Declaration of variable at describe level

    it("should register a new user successfully", async () => {
        const response = await register.registerUser(newUser);

        console.log("Response Status Code:", response.statusCode);
        console.log("Response Body:", response.body);

        checkStatusCode(response, 201); 

        // Assertions
        expect(response.body).to.have.property('user');
        expect(response.body.user).to.include.keys(['id', 'avatar', 'email', 'userName']);

        // Saving the user ID
        userId = response.body.user.id;
    });

    after(async () => {
        // User deletion
        if (userId) {
            await register.deleteUser(userId);
        }
    });
});
